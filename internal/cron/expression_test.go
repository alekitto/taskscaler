package cron

import (
	"testing"
	"time"
)

func check(pattern string, dates []string, contains bool) func(t *testing.T) {
	return func(t *testing.T) {
		t.Helper()

		cron, err := Parse(pattern, time.UTC.String())
		if err != nil {
			t.Fatalf("cron: %q, %v", pattern, err)
		}

		for _, d := range dates {
			dt, err := time.Parse("2006-01-02 15:04", d)
			if err != nil {
				t.Fatal(err)
			}

			if cron.Contains(dt) != contains {
				if contains {
					t.Errorf("expected %s to be contained within %s (%s)", dt, pattern, cron)
				} else {
					t.Errorf("expected %s to not be contained within %s (%s)", dt, pattern, cron)
				}
			}
		}
	}
}

type y []string
type n []string

func contains(t *testing.T, name string, pattern string, dates ...interface{}) {
	t.Helper()

	t.Run(name, func(t *testing.T) {
		for _, d := range dates {
			switch v := d.(type) {
			case y:
				check(pattern, v, true)(t)
			case n:
				check(pattern, v, false)(t)
			}
		}
	})
}

func yes(does ...string) y {
	return does
}

func not(does ...string) n {
	return does
}

// Tests are based on:
// https://github.com/cloudflare/saffron/blob/bb34024be42c735a4f48e18b9eafc3aac455e9c6/saffron/src/lib.rs#L1558
// but modified for standard-cron, rather than quartz.
func TestContains(t *testing.T) {
	contains(t, "parse_check_anytime", "* * * * *",
		yes("1970-01-01 00:00",
			"2016-11-08 23:53",
			"2020-07-04 15:42",
			"2072-02-29 01:15"))

	contains(t, "parse_check_specific_time", "5 0 23 8 *",
		yes("2020-08-23 00:05",
			"2021-08-23 00:05",
			"2022-08-23 00:05",
			"2023-08-23 00:05"),
		not("1970-01-01 00:00",
			"2016-11-08 23:53",
			"2020-07-04 15:42",
			"2072-02-29 01:15",
			"2020-08-23 11:05"))

	contains(t, "parse_check_specific_time_as_ranges", "5-5 0-0 23-23 8-8 *",
		yes("2020-08-23 00:05",
			"2021-08-23 00:05",
			"2022-08-23 00:05",
			"2023-08-23 00:05"),
		not("1970-01-01 00:00",
			"2016-11-08 23:53",
			"2020-07-04 15:42",
			"2072-02-29 01:15",
			"2020-08-23 11:05"))

	contains(t, "parse_check_overflow_time_ranges", "59-0 23-0 31-1 12-1 *",
		yes("2020-01-31 00:59",
			"2020-01-31 00:00",
			"2020-01-31 23:59",
			"2020-01-31 23:00",
			"2020-01-01 00:59",
			"2020-01-01 00:00",
			"2020-01-01 23:59",
			"2020-01-01 23:00",
			"2020-12-31 00:59",
			"2020-12-31 00:00",
			"2020-12-31 23:59",
			"2020-12-31 23:00",
			"2020-12-01 00:59",
			"2020-12-01 00:00",
			"2020-12-01 23:59",
			"2020-12-01 23:00"))

	contains(t, "parse_check_overflow_time_ranges", "0 0 * JAN SAT-SUN",
		yes("2020-01-04 00:00",
			"2020-01-05 00:00",
			"2020-01-11 00:00",
			"2020-01-12 00:00"))

	contains(t, "parse_check_limits", "0,59 0,23 1,31 1,12 *",
		yes("2020-01-01 00:00",
			"2020-01-01 00:59",
			"2020-01-01 23:59",
			"2020-01-31 23:59",
			"2020-12-31 23:59"))

	contains(t, "parse_check_anytime_but_its_ranges", "0-59 0-23 1-31 1-12 *",
		yes("1970-01-01 00:00",
			"2016-11-08 23:53",
			"2020-07-04 15:42",
			"2072-02-29 01:15"))

	contains(t, "parse_check_anytime_but_its_ranges", "0-59 0-23 * 1-12 1-7",
		yes("1970-01-01 00:00",
			"2016-11-08 23:53",
			"2020-07-04 15:42",
			"2072-02-29 01:15"))

	contains(t, "parse_check_steps", "*/15,30-59/10 0 * * *",
		yes("2020-01-01 00:00",
			"2020-01-01 00:15",
			"2020-01-01 00:30",
			"2020-01-01 00:40",
			"2020-01-01 00:45",
			"2020-01-01 00:50"))

	contains(t, "parse_check_overflow_range_step", "0 20-4/2 * * *",
		yes("2020-01-01 20:00",
			"2020-01-01 22:00",
			"2020-01-01 00:00",
			"2020-01-01 02:00",
			"2020-01-01 04:00"))
}
