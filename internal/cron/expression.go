package cron

import (
	"strconv"
	"strings"
	"time"
)

type Schedule interface {
	Contains(time.Time) bool
	String() string
}

type schedule struct {
	loc    *time.Location
	minute Expr
	hour   Expr
	dom    Expr
	month  Expr
	dow    Expr
}

func (s schedule) Contains(t time.Time) bool {
	t = t.In(s.loc)

	match := s.minute.match(Value(t.Minute())) &&
		s.hour.match(Value(t.Hour())) &&
		s.dom.match(Value(t.Day())) &&
		s.month.match(Value(t.Month()))

	if !match {
		return false
	}

	// handle sunday also being represented by a 7
	wd := t.Weekday()
	if wd == time.Sunday {
		return s.dow.match(Value(7)) || s.dow.match(Value(wd))
	}
	return s.dow.match(Value(wd))
}

func (s schedule) String() string {
	return s.minute.String() + " " +
		s.hour.String() + " " +
		s.dom.String() + " " +
		s.month.String() + " " +
		s.dow.String()
}

type (
	Expr interface {
		match(Value) bool
		String() string
	}
	Many  []Expr
	Range struct {
		From Expr
		To   Expr
	}
	Step struct {
		Range Expr
		Step  Value
		Max   Value
	}
	All   struct{}
	Value uint8
)

var (
	minuteExpr = field{0, 59, nil}
	hourExpr   = field{0, 23, nil}
	domExpr    = field{1, 31, nil}
	monthExpr  = field{1, 12, map[string]Value{
		"jan": 1, "feb": 2, "mar": 3,
		"apr": 4, "may": 5, "jun": 6,
		"jul": 7, "aug": 8, "sep": 9,
		"oct": 10, "nov": 11, "dec": 12,
	}}
	dowExpr = field{0, 7, map[string]Value{
		"sun": 0, "mon": 1, "tue": 2,
		"wed": 3, "thu": 4, "fri": 5,
		"sat": 6,
	}}
)

func (e Many) match(v Value) bool {
	for _, expr := range e {
		if expr.match(v) {
			return true
		}
	}

	return false
}

func (e Many) String() string {
	var str []string
	for _, expr := range e {
		str = append(str, expr.String())
	}

	return strings.Join(str, ",")
}

func (e Range) match(v Value) bool {
	from := e.From.(Value)
	to := e.To.(Value)

	negate := from > to

	if v < from {
		return negate
	}
	if v > to {
		return negate
	}

	return !negate
}

func (e Range) String() string {
	return e.From.String() + "-" + e.To.String()
}

func (e Step) match(v Value) bool {
	return e.Range.match(v) && v%e.Step == 0
}

func (e Step) String() string {
	return e.Range.String() + "/" + strconv.FormatUint(uint64(e.Step), 10)
}

func (e All) match(_ Value) bool {
	return true
}

func (e All) String() string {
	return "*"
}

func (e Value) match(v Value) bool {
	return e == v
}

func (e Value) String() string {
	return strconv.FormatUint(uint64(e), 10)
}
