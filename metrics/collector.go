package metrics

import (
	"time"
)

const (
	ScaleOperationUp   = "up"
	ScaleOperationDown = "down"
)

const (
	TaskOperationAcquire                         = "acquire"
	TaskOperationAcquireTimeout                  = "acquire_timeout"
	TaskOperationRelease                         = "release"
	TaskOperationReserve                         = "reserve"
	TaskOperationReserveCapacityFailure          = "reserve_iop_capacity_failure" // IoP => ImmediateOrPotential
	TaskOperationReserveAvailableCapacityFailure = "reserve_available_capacity_failure"
	TaskOperationUnreserve                       = "unreserve"
)

const (
	TasksCountIdle        = "idle"
	TasksCountPending     = "pending"
	TasksCountAcquired    = "acquired"
	TasksCountReserved    = "reserved"
	TasksCountUnavailable = "unavailable"
)

type Collector interface {
	MaxUseCountSet(v int)
	MaxTasksPerInstanceSet(v int)

	ScaleOperationInc(operation string)

	TaskOperationInc(operation string)
	TasksCountSet(state string, v int)

	TaskInstanceReadinessTimeObserve(d time.Duration)

	DesiredInstancesSet(v int)
}

func Init(mc Collector, maxUseCount, capacityPerInstance int) Collector {
	if mc == nil {
		return nil
	}

	mc.MaxUseCountSet(maxUseCount)
	mc.MaxTasksPerInstanceSet(capacityPerInstance)

	mc.TasksCountSet(TasksCountIdle, 0)
	mc.TasksCountSet(TasksCountPending, 0)
	mc.TasksCountSet(TasksCountAcquired, 0)
	mc.TasksCountSet(TasksCountReserved, 0)
	mc.TasksCountSet(TasksCountUnavailable, 0)

	return mc
}
