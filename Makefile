test:
	go test ./...
	@$(MAKE) mock
	@git --no-pager diff --compact-summary --exit-code -- ./mocks && echo 'Mocks are up-to-date!'
	@git --no-pager diff --compact-summary --exit-code -- go.mod go.sum && echo 'Go modules are tidy and complete!'

mock:
	@go install github.com/vektra/mockery/v2@v2.42.2
	@mockery --with-expecter --name=Acquisition
	@mockery --with-expecter --name=Taskscaler
	@mockery --with-expecter --keeptree --srcpkg ./metrics --name=Collector
