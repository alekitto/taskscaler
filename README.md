# Taskscaler

Taskscaler is an autoscaler for provisioning instances (via fleeting) and
allocating and assigning tasks to them.

Each instance can be virtually configured for a certain capacity for tasks, and
the autoscaler will try to ensure that there's always enough capacity to meet
demand and scale back when required.

To keep things incredibly simple, each task is assumed to be the same size.
Giving each instance a capacity of 12 means that it can run 12 tasks. How an
instance isolates tasks or prevents noisy neighbours is outside the scope of
this library. Taskscaler handles the book-keeping only: if you ask it for an
instance with capacity, it will provide one.